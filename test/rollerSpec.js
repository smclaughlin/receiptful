var expect = require('chai').expect;
var roller = require('../lib/roller.js');

describe('Roller', function(){
  it('should return undefined if an array isnt passed', function(){
    var result = roller.roll(1);

    expect(result).to.be.undefined;
  })

  it('should return undefined if an empty array is passed', function(){
    var array = [];

    var result = roller.roll(array);

    expect(result).to.eql([]);
  });

  it('should return the arg if it is the only value in the array', function(){
    var array = [1];

    var result =  roller.roll(array);

    expect(result).to.eql(array);
  });

  it('should return a flat array when passed a flat array', function(){
    var array = [1,2,3,4];

    var result = roller.roll(array);

    expect(result).to.eql(array);
  });

  it('should return a flat array when a nested array is passed', function(){
    var array = [1,6,[2,3],4];

    var result = roller.roll(array);

    expect(result).to.eql([1,6,2,3,4]);
  });

  //test case from question
  it('should return a flat array when passed a more complex nested array', function(){
    var array = [[1,2,[3]],4] 

    var result = roller.roll(array);

    expect(result).to.eql([1,2,3,4]);
  });

  it('should return a flat array when a much more complex nested array is passed', function(){
    var array = [[],[1,2,[3,[4,[5,6]]]],7,[]] 

    var result = roller.roll(array);

    expect(result).to.eql([1,2,3,4,5,6,7]);
  });
})