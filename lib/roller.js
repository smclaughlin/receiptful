'use strict'

var roll = function(values) {
  if(!Array.isArray(values)){
    return;
  }

  var retVal = [];

  var i = 0;

  for(var i = 0, j = values.length; i < j  ;i++) {
    var val = values[i];

    if(Array.isArray(val)) {
            retVal = retVal.concat(roll(val));
        } else {
            retVal.push(val);
        }
    }

  return retVal;
}


module.exports = {roll: roll};

